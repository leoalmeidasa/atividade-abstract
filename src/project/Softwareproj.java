
package project;


public class Softwareproj extends Project{
    protected double LicensesCost;

    public Softwareproj(String Name, double Labourhour, double LicensesCost) {
        super(Name, Labourhour);
        this.LicensesCost = LicensesCost;
    }

    @Override
    public double Currentcost() {
        return (this.Labourhour*this.Hour)+this.LicensesCost;
    }

    @Override
    public String Info() {
        return "Name:"+this.Name+"\n"+
               "CostHour:"+this.Labourhour+"\n"+
               "Hour:"+this.Hour+"\n"+
               "CurrentCost:"+this.Currentcost();
    }
    
}
