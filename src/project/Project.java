package project;

public abstract class Project {

    protected String Name;
    protected double Labourhour;
    protected double Hour;

    public Project(String Name, double Labourhour) {
        this.Name=Name;
        this.Labourhour=Labourhour;
        this.Hour=0;
    }
    
    protected void AddHours(double Hour){
        this.Hour+=Hour;
    }
    
    protected abstract double Currentcost();

    protected abstract String Info();

}
