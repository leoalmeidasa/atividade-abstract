
package project;


public class Hardwareproj extends Project {
    protected double ComponentsCost;

    public Hardwareproj(String Name, double Labourhour, double ComponentsCost) {
        super(Name, Labourhour);
        this.ComponentsCost = ComponentsCost;
    }

    @Override
    public double Currentcost() {
        return (this.Labourhour*this.Hour)+this.ComponentsCost;
    }

    @Override
    public String Info() {
        return "Name:"+this.Name+"\n"+
               "CostHour:"+this.Labourhour+"\n"+
               "Hour:"+this.Hour+"\n"+
               "CurrentCost:"+this.Currentcost();
    }
    
}
